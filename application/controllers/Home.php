<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
  public function index()
  {
    $headerData['pageTitle'] = PAGE_TITLE;

    $homeData['products'] = array(
        array('title' => 'Product No. 1', 'description' => 'Lorem Sum'),
        array('title' => 'Product No. 2', 'description' => 'Lorem Sum'),
        array('title' => 'Product No. 3', 'description' => 'Lorem Sum'),
        array('title' => 'Product No. 4', 'description' => 'Lorem Sum'),
        array('title' => 'Product No. 5', 'description' => 'Lorem Sum'),
    );


    $this->load->view('includes/header', $headerData);
    $this->load->view('pages/homepage', $homeData);
    $this->load->view('includes/footer');
  }
}
