

<style type="text/css">

      @keyframes productEnter {
        0% { transform: translateY(30px); opacity: 0; }
        80% { transform: translateY(-20px); opacity: 1; }
        100% { transform: translateY(0px); opacity: 1; }
      }
      .products {
        display: grid;
        grid-template-columns: repeat(5, 1fr);
        grid-gap: 20px;
        margin: 20px 0;
      }

      .products .item {
          animation: productEnter 0.5s;
          background: grey;
      }
</style>
<div class="products">
  <?php foreach($products as $i => $product):  ?>
    <div class="item" style="animation-delay: <?= 0.1*$i?>s;">
      <div class="title"><?= $product['title'] ?></div>
      <div class="description"><?= $product['description'] ?></div>
    </div> 
  <?php endforeach ?>
</div>
</p>

</h1>